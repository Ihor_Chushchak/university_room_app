package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Address {
    private String country;
    private String city;
    private String street;
    private String number;

    public String toString() {
        return country + ", " + city + ", " + street + " " + number;
    }
}
