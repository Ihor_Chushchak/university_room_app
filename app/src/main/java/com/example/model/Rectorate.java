package com.fedchuk.model;

import lombok.Data;

@Data
public class Rectorate {
    private String name;
    private String report;

    public Rectorate(String name) {
        this.name = name;
    }
}
