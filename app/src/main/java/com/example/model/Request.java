package com.example.model;

import com.example.model.enums.RequestStatus;

import java.time.LocalDate;
import java.time.LocalTime;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Request {
    private Room room;
    private LocalDate date;
    private LocalTime beginTime;
    private LocalTime endTime;
    private User user;
    private RequestStatus requestStatus;

    public Request(Room room, LocalDate date, LocalTime beginTime, LocalTime endTime, User user) {
        this.date = date;
        this.room = room;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.user = user;
    }

    public Request(Room room, User user, LocalDate date) {
        this.room = room;
        this.user = user;
        this.date = date;
    }

    public void setStatus(RequestStatus status) {
        requestStatus = status;
    }
}
