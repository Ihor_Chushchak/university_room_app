package com.example.model;

import com.fedchuk.model.enums.RoomType;
import com.fedchuk.model.enums.Status;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Room {
    private Address address;
    private String educationalBuilding;
    private String number;
    private RoomType roomType;
    private String sanitaryCondition;
    private String fireCondition;
    private Status status;

    public Room() {
        this.roomType = RoomType.AUDITORIUM;
        this.status = Status.NORMAL;
    }

    @Override
    public String toString() {
        return "address=" + address +
                ", educationalBuilding='" + educationalBuilding + '\'' +
                ", number='" + number + '\'' +
                ", roomType=" + roomType;
    }
}
