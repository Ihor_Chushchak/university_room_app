package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class User {
    protected String name;
    protected String login;
    protected String password;
    protected String secretWord;

    public boolean isAccountVerified(String login, String password) {
        return this.login.equals(login) && this.password.equals(password);
    }

    @Override
    public String toString() {
        return name;
    }
}
