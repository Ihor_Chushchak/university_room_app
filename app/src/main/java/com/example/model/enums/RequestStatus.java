package com.example.model.enums;

public enum RequestStatus {
    ACCEPTED, DENIED, DISMISSED, PENDING;
}
