package com.fedchuk.model.enums;

public enum RoomType {
    AUDITORIUM, CLASSROOM, LABORATORY;
}
