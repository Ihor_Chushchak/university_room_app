package com.fedchuk.model.enums;

public enum Status {
    PERFECT, GOOD, NORMAL, BAD, AWFUL;
}
