package com.example.roomacc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class AddingTextActivity extends AppCompatActivity {
    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);
        }
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // only portrait
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addtext);
        addOnSaveButtonListener();
        addOnSwitchListener();
    }

    private void addOnSaveButtonListener() {
        final Button saveButton = findViewById(R.id.save);
        final EditText textArea = findViewById(R.id.textArea);
        saveButton.setOnClickListener((v)->{

        });
    }

    private void addOnSwitchListener() {
        Switch modeSwitcher = findViewById(R.id.modeA);
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            modeSwitcher.setChecked(true);
        }
        modeSwitcher.setOnCheckedChangeListener((b,checked) -> {
            if(checked){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                startActivity(new Intent(getApplicationContext(), AddingTextActivity.class));
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                startActivity(new Intent(getApplicationContext(), AddingTextActivity.class));
            }
            finish();
        });
    }
}
