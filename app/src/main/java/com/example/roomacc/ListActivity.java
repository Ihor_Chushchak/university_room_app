package com.example.roomacc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class ListActivity extends AppCompatActivity {
    String name[] = {"fddf", "fdfdf", "gfgfg", "fddsgfd", "hello", "bye", "mir"};
    int[] colors = new int[]{Color.WHITE, Color.WHITE};

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); // only!
        setRooms("");
        addOnSearchListener();
        addOnAddButtonListener();
        addOnUpdateButtonListener();
        addOnDeleteButtonListener();
    }

    private void addOnDeleteButtonListener() {
        Button deleteButton = findViewById(R.id.delete);
        deleteButton.setOnClickListener((v)->{

        });
    }

    private void addOnAddButtonListener() {
        Button addButton = findViewById(R.id.add);
        addButton.setOnClickListener((v)->{
            startActivity(new Intent(ListActivity.this, AddingTextActivity.class));
        });
    }

    private void addOnUpdateButtonListener() {
        Button updateButton = findViewById(R.id.update);
        updateButton.setOnClickListener((v)->{
            startActivity(new Intent(ListActivity.this, AddingTextActivity.class));
        });
    }

    private void setRooms(String searchContent){
        LinearLayout linLayout = findViewById(R.id.linLayout);
        for (String s : name) {
            if (isFoundSomeThing(s, searchContent)) {
                View item = getLayoutInflater().inflate(R.layout.item, linLayout, false);
                TextView textName = item.findViewById(R.id.textName);
                TextView author = item.findViewById(R.id.author);
                TextView date = item.findViewById(R.id.date);
                item.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
                linLayout.addView(item);
            }
        }
    }

    private void addOnSearchListener(){
        SearchView search = findViewById(R.id.search);
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                System.out.println("HERE2");
                LinearLayout linLayout = findViewById(R.id.linLayout);
                linLayout.removeAllViewsInLayout();
                setRooms(newText);
                return false;
            }
        });
    }

    private boolean isFoundSomeThing(String whereToFind, String whatToFind){
        return whereToFind.contains(whatToFind);
    }
}
