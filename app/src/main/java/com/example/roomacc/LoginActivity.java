package com.example.roomacc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class LoginActivity extends AppCompatActivity {
    private Switch modeSwitcher;
    private EditText loginField;
    private EditText passwordField;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);
        }
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // only portrait
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        addOnSubmitButtonListener();
        addOnSwitchListener();
        addRegisterListener();
        addResetPasswordListener();
    }

    @SuppressLint("ShowToast")
    private void addOnSubmitButtonListener() {
        Button submit = findViewById(R.id.submit);
        final ImageView locker = findViewById(R.id.locker);
        submit.setOnClickListener((v)-> {
            if(check()){
                locker.setImageResource(R.drawable.unlocked);
                Toast.makeText(LoginActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            } else {
                Toast.makeText(LoginActivity.this, "Wrong login or password",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addRegisterListener() {
        final Button register = findViewById(R.id.reset);
        register.setOnClickListener((v)-> startActivity(new Intent(LoginActivity.this, RegisterActivity.class)));
    }

    private void addResetPasswordListener() {
        final TextView resetPassword = findViewById(R.id.resetPassword);
        resetPassword.setOnClickListener((v)-> startActivity(new Intent(LoginActivity.this, ResetPasswordActivity.class)));
    }


    private void addOnSwitchListener() {
        modeSwitcher = findViewById(R.id.mode);
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            modeSwitcher.setChecked(true);
        }
        modeSwitcher.setOnCheckedChangeListener((b,checked) -> {
            if(checked){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
            finish();
        });
    }

    private boolean check() {
        loginField = findViewById(R.id.login);
        passwordField = findViewById(R.id.password);
        return true;
    }

}
