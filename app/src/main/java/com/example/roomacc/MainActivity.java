package com.example.roomacc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private Random random = new Random();
    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);
        }
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // only portrait
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        addOnCompareButtonListener();
        addOnSwitchListener();
        addOnAdminButtonListener();
        addOnSaveBUttonListener();
    }

    private void addOnSaveBUttonListener() {
        final Button saveButton = findViewById(R.id.saveM);
        saveButton.setOnClickListener((v)->{
            Toast.makeText(MainActivity.this, "Nothing to save", Toast.LENGTH_SHORT).show();
        });
    }

    private void addOnAdminButtonListener() {
        final Button adminButton = findViewById(R.id.admin);
        adminButton.setOnClickListener((v)->{
            startActivity(new Intent(MainActivity.this, ListActivity.class));
        });
    }

    @SuppressLint("SetTextI18n")
    private void addOnCompareButtonListener() {
        final Button compareButton = findViewById(R.id.compare);
        final ProgressBar progressBar = findViewById(R.id.progressBar);
        TextView progress = findViewById(R.id.progress);
        compareButton.setOnClickListener((v)->{
            int percentage = random.nextInt(101);
            progressBar.setProgress(percentage);
            progress.setText(percentage+"%");
        });
    }

    private void addOnSwitchListener() {
        Switch modeSwitcher = findViewById(R.id.modeM);
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            modeSwitcher.setChecked(true);
        }
        modeSwitcher.setOnCheckedChangeListener((b,checked) -> {
            if(checked){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
            finish();
        });
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBackPressed(){
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage("Do you really want to log out?")
                .setPositiveButton("Yes",(d,w)->{
                    Toast.makeText(MainActivity.this, "Logging out", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }).setNegativeButton("No", null).show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(R.color.black);
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(R.color.black);
    }
}
