package com.example.roomacc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class RegisterActivity extends AppCompatActivity {

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);
        }
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // only portrait
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        addOnSubmitButtonListener();
        addOnSwitchListener();
        addResetTextListener();
    }

    @SuppressLint("ShowToast")
    private void addOnSubmitButtonListener() {
        Button submit = findViewById(R.id.submitR);
        submit.setOnClickListener((v)-> {
            if(check()){
                Toast.makeText(RegisterActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            } else {
                Toast.makeText(RegisterActivity.this, "Wrong data entered!", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addResetTextListener() {
        final Button reset = findViewById(R.id.reset);
        EditText loginField = findViewById(R.id.loginR);
        EditText passwordField = findViewById(R.id.passwordR);
        EditText nameField = findViewById(R.id.nameR);
        EditText secretWordField = findViewById(R.id.SecretWordR);
        reset.setOnClickListener((v)->{
            loginField.setText("");
            passwordField.setText("");
            nameField.setText("");
            secretWordField.setText("");
        });
    }

    private void addOnSwitchListener() {
        Switch modeSwitcher = findViewById(R.id.modeR);
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            modeSwitcher.setChecked(true);
        }
        modeSwitcher.setOnCheckedChangeListener((b, checked) -> {
            if(checked){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
            finish();
        });
    }

    private boolean check() {
        EditText loginField = findViewById(R.id.loginR);
        EditText passwordField = findViewById(R.id.passwordR);
        EditText nameField = findViewById(R.id.nameR);
        EditText secretWordField = findViewById(R.id.SecretWordR);
        return nameField.getText().length() >= 6;
    }
}
