package com.example.roomacc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class ResetPasswordActivity extends AppCompatActivity {
    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);
        }
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // only portrait
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restorepassword);
        addOnSubmitButtonListener();
    }

    @SuppressLint("ShowToast")
    private void addOnSubmitButtonListener() {
        Button submit = findViewById(R.id.submitRP);
        submit.setOnClickListener((v)-> {
            if(check()){
                Toast.makeText(ResetPasswordActivity.this, "Password has been successfully restored",
                        Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ResetPasswordActivity.this, LoginActivity.class));
            } else {
                Toast.makeText(ResetPasswordActivity.this, "Wrong data entered!", Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean check() {
        EditText newPasswordField = findViewById(R.id.newPasswordRP);
        EditText nameField = findViewById(R.id.nameRP);
        EditText secretWordField = findViewById(R.id.SecretWordRP);
        return nameField.getText().length() >= 6;
    }
}
