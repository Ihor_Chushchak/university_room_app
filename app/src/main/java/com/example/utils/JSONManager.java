package com.example.utils;

import com.example.model.Request;
import com.example.model.Room;
import com.example.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

//Запис та читання приміщень, запитів і користувачів
public class JSONManager {
    private static final Gson GSON = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
    private static final String BASE_DATA_PATH = "src/main/res/data";

    public static Map<User, Boolean> getUsers() {
        try (FileReader reader = new FileReader(BASE_DATA_PATH + "/users.json")) {
            Type mapType = new TypeToken<Map<User, Boolean>>() {}.getType();
            return GSON.fromJson(reader, mapType);
        } catch (IOException e) {
            e.printStackTrace();
            return new LinkedHashMap<>();
        }
    }

    public static void writeUsers(Map<User, Boolean> users) {
        try (FileWriter writer = new FileWriter(BASE_DATA_PATH + "/users.json")) {
            Type mapType = new TypeToken<Map<User, Boolean>>() {}.getType();
            GSON.toJson(users, mapType, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Room> getRooms() {
        try (FileReader reader = new FileReader(BASE_DATA_PATH + "/list.json")) {
            Type listType = new TypeToken<List<Room>>() {}.getType();
            return GSON.fromJson(reader, listType);
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public static void writeRooms(List<Room> rooms) {
        try (FileWriter writer = new FileWriter(BASE_DATA_PATH + "/list.json")) {
            Type listType = new TypeToken<List<Room>>() {}.getType();
            GSON.toJson(rooms, listType, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Request> getRequests() {
        try (FileReader reader = new FileReader(BASE_DATA_PATH + "/requests.json")) {
            Type listType = new TypeToken< List<Request>>() {}.getType();
            return GSON.fromJson(reader, listType);
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public static void writeRequests(List<Request> requests) {
        try (FileWriter writer = new FileWriter(BASE_DATA_PATH + "/requests.json")) {
            Type listType = new TypeToken< List<Request>>() {}.getType();
            GSON.toJson(requests, listType, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
