package com.example.utils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class MenuManager {

    public static Map<String, String> getMainMenu(String bundleName) {
        ResourceBundle bundle = ResourceBundle.getBundle(bundleName);
        Map<String, String> menu = new LinkedHashMap<>();
        int countOptions = Integer.parseInt(bundle.getString("countOptions"));
        for (int i = 1; i <= countOptions; i++) {
            menu.put(i + "", bundle.getString(i + ""));
        }
        menu.put("Q", bundle.getString("Q"));
        return menu;
    }

    public static Map<String, String> getStartMenu() {
        Map<String, String> menu = new LinkedHashMap<>();
        menu.put("1", "Create an account.");
        menu.put("2", "Log in.");
        menu.put("Q", "Quit.");
        return menu;
    }
}
