package com.example.utils;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.time.LocalTime;

//Клас для перевірки коректності вхідних даних
public class ValidationManager {

    public static boolean isValidName(String name) {
        return name.matches("^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$")&& !name.isEmpty();
    }

    public static boolean isValidStreetNumber(String streetNumber) {
        return streetNumber.matches("^[0-9]{0,3}[a-zA-Z]?$") && !streetNumber.isEmpty();
    }

    public static boolean isValidBuilding(String building) {
        return building.matches("^(main)?[0-9]{0,2}[a-zA-Z]?$") && !building.isEmpty();
    }

    public static boolean isValidRoomNumber(String streetNumber) {
        return streetNumber.matches("^[1-9][0-9]{2}$") && !streetNumber.isEmpty();
    }

    public static boolean isValidText(String text) {
        return text.matches("^[a-zA-Z0-9.,\\s]*$") && !text.isEmpty();
    }

    public static boolean isValidTime(String time) {
        return time.matches("([01]?[0-9]|2[0-3]):[0-5][0-9]") && !time.isEmpty();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static boolean isValidTime(String begin, String end) {
        boolean result = isValidTime(begin) && isValidTime(end);
        if(result){
            LocalTime beginT = LocalTime.parse(begin);
            LocalTime endT = LocalTime.parse(end);
            result = (endT.getHour() * 60 + endT.getMinute()) - beginT.getHour() * 60 - beginT.getMinute() > 0;
        }
        return result;
    }

    public static boolean isValidLogin(String login) {
        return login.matches("^[\\w.-]{0,19}[0-9a-zA-Z]$") && !login.isEmpty();
    }

    public static boolean isValidUserName(String name) {
        return name.matches("^[a-zA-Z]{3,20}$") && !name.isEmpty();
    }

    public static boolean isValidPassword(String password) {
        return password.matches("^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$") && !password.isEmpty();
    }

}
